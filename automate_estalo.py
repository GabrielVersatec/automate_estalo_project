import os


class Cursor:

    def __init__(self):
        self.pathBASE = r'C:\Users\biels\Documents\estalo-next' # Substitua aqui
        self.pathAPI = r'C:\Users\biels\Documents\estalo-next/packages/tnl/api/handler' # Substitua aqui
        self.pathAPP = r'C:\Users\biels\Documents\estalo-next/packages/app' # Substitua aqui
        self.choice = ''

    def what_should_i_have_to_do(self):
        print('======= ESTALO 2 =======\n')
        self.choice = int(input('(1) - Iniciar \n(2) - Atualizar\n'))

        if self.choice == 1:
            self.init_server_estalo()
        elif self.choice == 2:
            self.update_estalo()
        else:
            print('\nSelecione uma das opções. Tente novamente.\n')
            os.system('pause')
            self.what_should_i_have_to_do()

    def init_server_estalo(self):
        try:
            cmd = 'start cmd /k; pnpm serve'
            print(os.getcwd())
            os.chdir(self.pathAPI)
            os.system(cmd)
            os.chdir(self.pathAPP)
            os.system(cmd)

        except:
            print('Something Wrong')
            pass

    def update_estalo(self):
        try:
            cmd = 'start cmd /k; git pull'
            print(os.getcwd())
            os.chdir(self.pathBASE)
            os.system(cmd)

        except:
            print('Something Wrong')
            pass


if __name__ == '__main__':
    obj = Cursor()
    obj.what_should_i_have_to_do()

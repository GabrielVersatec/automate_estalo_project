# Features

- Levanta o servidor local automaticamente
- Atualiza o projeto

# Observações

É necessário alterar as variáveis **pathBASE**, **pathAPI** e **pathAPP** de acordo com o caminho onde você instalou a aplicação.


1. pathBASE: Armazena o diretório base do estalo. 
EX: 'C:/meuPerfil/documentos/estalo' (É onde consigo realizar o git pull, pnpm install etc)

2. pathAPI: Armazena o diretório da API do estalo. 
EX: 'C:/meuPerfil/documentos/estalo/packages/tnl/api/handler' (É onde roda o comando pnpm serve)

3. pathBASE: Armazena o diretório base da aplicação do estalo. 
EX: 'C:/meuPerfil/documentos/estalo/packages/app' (É onde roda o comando pnpm serve)
